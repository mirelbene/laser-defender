﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreDes : MonoBehaviour
{

    Text scoreText;
    GameSession gameSession;


    void Start()
    {
        scoreText = GetComponent<Text>();
       
    }

    // Update is called once per frame
    void Update()
    {
        gameSession = FindObjectOfType<GameSession>();
        scoreText.text = gameSession.GetScore().ToString();
    }
}
